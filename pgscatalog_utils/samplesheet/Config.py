from dataclasses import dataclass


@dataclass
class Config:
    input_path: str
    output_path: str
